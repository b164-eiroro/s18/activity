console.log('Welcome to Pokemon Battle!')
let player = {};
player.name = "Skailerdan";
player.age = 24;
player.friends = {
		hoenn: ["Flannery", "Winona"],
		kanto: ["Brock", "Misty"]
}
player.pokemon = ["Ho-oh", "Kyogre", "Lugia", "Raikou"]
player.talk = function() {
	console.log(`Hello my name is ${player.name}! I wanna be the very best!`)
}
console.log(player)


console.log("Result of dot notation:")
console.log(player.name)

console.log("Result of bracket notation:")
console.log(player['pokemon'])

console.log("Result talk method:")
player.talk()


function Pokemon(name, level, health, attack) {
	this.name = name
	this.level = level
	this.health = 3 * level
	this.attack = 1.5 * level

	this.tackle = function(target) {
		damage =  target.health - this.attack 
		console.log(`${this.name} tackled ${target.name}`);
		console.log(`${target.name}'s health is now reduced to ${damage}`)
		if (target.health < 0) {
			target.faint()
		}


	}
	this.faint = function() {
		console.log(`${target.name} fainted.`)
	}
}

let hooh = new Pokemon("Ho-oh", 80)
let kyo = new Pokemon("Kyogre", 40)
let lug = new Pokemon("Lugia", 60)
let rai = new Pokemon("Raikou", 100)